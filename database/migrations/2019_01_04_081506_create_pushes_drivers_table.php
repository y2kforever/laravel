<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePushesDriversTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pushes_drivers', function (Blueprint $table) {
            $table->integer('push_id')->unsigned();
            $table->integer('driver_id')->unsigned();
            $table->foreign('push_id')->references('id')->on('pushes')->onDelete('CASCADE');
            $table->foreign('driver_id')->references('id')->on('drivers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pushes_drivers');
    }
}
