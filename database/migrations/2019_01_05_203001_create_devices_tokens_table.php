<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDevicesTokensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('devices_tokens', function (Blueprint $table) {
            $table->integer('device_id')->unsigned();
            $table->integer('token_id')->unsigned();
            $table->foreign('device_id')->references('id')->on('devices');
            $table->foreign('token_id')->references('id')->on('tokens')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('devices_tokens');
    }
}
