<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewsByGeoZonesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('news_by_geo_zones', function (Blueprint $table) {
            $table->integer('geo_zone_id')->unsigned();
            $table->integer('news_id')->unsigned();
            $table->foreign('geo_zone_id')->references('id')->on('geo_zones')->onDelete('CASCADE');
            $table->foreign('news_id')->references('id')->on('news')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('news_by_geo_zones');
    }
}
