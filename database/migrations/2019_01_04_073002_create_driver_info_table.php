<?php

use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDriverInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('driver_info', function (Blueprint $table) {
            $table->integer('driver_id')->unsigned();
            $table->string('first_name', 80);
            $table->string('last_name', 80);
            $table->string('parent_name', 80)->nullable();
            $table->date('birth_date');
            $table->string('picture');
            $table->string('passport_number');
            $table->date('start_date')->default(Carbon::now());
            $table->date('end_date')->default('9999-12-31');
            $table->string('gender', 6)->default('male');
            $table->foreign('driver_id')->references('id')->on('drivers')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('driver_info');
    }
}
