<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDevicesAppsReleasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('devices_apps_releases', function (Blueprint $table) {
            $table->integer('device_id')->unsigned();
            $table->integer('app_id')->unsigned();
            $table->boolean('status')->default(true);
            $table->foreign('device_id')->references('id')->on('devices')->onDelete('CASCADE');
            $table->foreign('app_id')->references('id')->on('app_releases')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('devices_apps_releases');
    }
}
