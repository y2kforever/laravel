<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewsTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('news_translations', function (Blueprint $table) {
            $table->integer('news_id')->unsigned();
            $table->integer('lang_id')->unsigned();
            $table->string('title', 255);
            $table->text('description');
            $table->foreign('news_id')->references('id')->on('news')->onDelete('CASCADE');
            $table->foreign('lang_id')->references('id')->on('languages')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('news_translations');
    }
}
