<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDriverReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('driver_reports', function (Blueprint $table) {
            $table->integer('driver_id')->unsigned();
            $table->date('date');
            $table->smallInteger('order_count')->default(0);
            $table->smallInteger('missed_count')->default(0);
            $table->smallInteger('canceled_count')->default(0);
            $table->smallInteger('operator_canceled_count')->default(0);
            $table->decimal('sum_total')->default(0);
            $table->foreign('driver_id')->references('id')->on('drivers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('driver_reports');
    }
}
