<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_histories', function (Blueprint $table) {
            $table->integer('id')->unsigned();
            $table->string('name', 80);
            $table->integer('merchant_id')->unsigned();
            $table->integer('category_id')->unsigned();
            $table->double('price');
            $table->integer('currency_id')->unsigned();
            $table->integer('status_id')->unsigned();
            $table->double('volume');
            $table->double('weight');
            $table->string('picture', 180);
            $table->timestamp('created_at');
            $table->timestamp('updated_at')->nullable();
            $table->foreign('merchant_id')->references('id')->on('merchants');
            $table->foreign('category_id')->references('id')->on('categories');
            $table->foreign('currency_id')->references('id')->on('currencies');
            $table->foreign('status_id')->references('id')->on('product_statuses');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_histories');
    }
}
