<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMerchantSchedulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('merchant_schedules', function (Blueprint $table) {
            $table->integer('merchant_id')->unsigned();
            $table->string('name', 80);
            $table->boolean('lunch')->default(false);
            $table->smallInteger('week_day');
            $table->time('start_time');
            $table->time('end_time');
            $table->foreign('merchant_id')->references('id')->on('merchants');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('merchant_schedules');
    }
}
