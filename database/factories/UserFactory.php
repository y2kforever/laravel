<?php

use App\Entity\User;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {

    $active = $faker->boolean;

    return array(
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', // secret
        'remember_token' => str_random(10),
        'verify_token' => $active ? null : \Illuminate\Support\Str::uuid(),
        'status_id' => $active ? $faker->randomElement([
            User::STATUS_ACTIVE,
            User::STATUS_WAIT,
            User::STATUS_BLOCK
        ]) : User::STATUS_WAIT,
    );
});
