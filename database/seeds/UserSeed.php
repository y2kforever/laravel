<?php

use Illuminate\Database\Seeder;
use App\Entity\User;

class UserSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([
            'name' => 'Admin',
            'email' => 'admin@admin.com',
            'password' => bcrypt('password'),
            'status_id' => User::STATUS_ACTIVE
        ]);
        $user->assignRole(User::ROLE_ADMINISTRATOR);

    }
}
