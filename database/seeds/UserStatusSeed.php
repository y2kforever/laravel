<?php

use App\Entity\UserStatus;
use Illuminate\Database\Seeder;

class UserStatusSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        UserStatus::create(['name' => 'active', 'description' => 'Active status']);
        UserStatus::create(['name' => 'wait', 'description' => 'Inactive status']);
        UserStatus::create(['name' => 'block', 'description' => 'Blocked status']);

    }
}
