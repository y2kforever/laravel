<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;

class RoleSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::create(['name' => 'user']);
        $moderator = Role::create(['name' => 'moderator']);
        $administrator = Role::create(['name' => 'administrator']);
        $moderator->givePermissionTo('users_manage');
        $administrator->givePermissionTo('users_manage');
        $administrator->givePermissionTo('roles_manage');
        $administrator->givePermissionTo('permissions_manage');
    }
}
