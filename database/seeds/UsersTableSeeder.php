<?php
/**
 * Created by PhpStorm.
 * User: abdugania
 * Date: 2/01/19
 * Time: 10:41 PM
 */

use App\Entity\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    public function run(): void
    {
        factory(User::class, 10)->create();
    }
}