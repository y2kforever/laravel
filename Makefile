docker-up:
	docker-compose up -d
docker-down:
	docker-compose down
docker-build:
	docker-compose up --build -d
test:
	docker exec laravel_php-fpm_1 vendor/bin/phpunit --colors=always
create-user-seeds:
	docker exec laravel_php-fpm_1 php artisan db:seed --class=UsersTableSeeder
composer-autoload:
	docker exec laravel_php-fpm_1 php composer dump-autoload -o
assets-install:
	docker exec laravel_node_1 yarn install
assets-dev:
	docker exec laravel_node_1 yarn run dev
assets-prod:
	docker exec laravel_node_1 yarn run prod
assets-watch:
	docker exec laravel_node_1 yarn run watch

perm:
	sudo chown ${USER}:${USER} bootstrap/cache -R
	sudo chown ${USER}:${USER} storage -R
	if [ -d "node_modules" ]; then sudo chown ${USER}:${USER} node_modules -R; fi
	if [ -d "public/build" ]; then sudo chown ${USER}:${USER} public/build -R; fi
