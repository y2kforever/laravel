<?php
/**
 * Created by PhpStorm.
 * User: abdugania
 * Date: 18/12/18
 * Time: 4:00 PM
 */

namespace App\Http\Requests\Admin\Users;

use App\Entity\User;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

/**
 * Class UpdateRequest
 * @property User $user
 */
class UpdateRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users,id,' . $this->user->id,
            'status' => [
                'required',
                'string',
                Rule::in([
                        User::STATUS_WAIT,
                        User::STATUS_ACTIVE
                    ]
                )
            ],
        ];
    }
}