<?php
/**
 * Created by PhpStorm.
 * User: abdugania
 * Date: 19/12/18
 * Time: 12:59 AM
 */

namespace App\Http\Requests\Auth;

use Illuminate\Foundation\Http\FormRequest;

class LoginRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'email' => 'required|string',
            'password' => 'required|string',
        ];
    }
}