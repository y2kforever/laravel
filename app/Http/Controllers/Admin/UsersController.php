<?php

namespace App\Http\Controllers\Admin;

use App\Entity\User;
use Spatie\Permission\Models\Role;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Users\{CreateRequest, UpdateRequest};

class UsersController extends Controller
{
    public function index()
    {
        $statuses = [
            User::STATUS_WAIT => 'Waiting',
            User::STATUS_ACTIVE => 'Active',
        ];

        $users = User::orderBy('id', 'desc')->paginate(20);
        $roles = Role::all()->toArray();

        return view('admin.users.index', compact('users', 'statuses', 'roles'));
    }

    public function create()
    {
        $roles = Role::get()->pluck('name', 'name');
        return view('admin.users.create', compact('roles'));

    }

    public function store(CreateRequest $request)
    {
        $user = User::new(
            $request['name'],
            $request['email']
        );

        return redirect()->route('admin.users.show', $user);
    }

    public function show(User $user)
    {
        return view('admin.users.show', compact('user'));
    }

    public function edit(User $user)
    {
        $statuses = [
            User::STATUS_WAIT => 0,
            User::STATUS_ACTIVE => 1,
        ];

        $roles = Role::get()->pluck('name', 'id');

        return view('admin.users.edit', compact('user', 'statuses', 'roles'));
    }

    public function update(UpdateRequest $request, User $user)
    {

        $user->update($request->only(['name', 'email']));

        return redirect()->route('admin.users.show', $user);
    }

    public function destroy(User $user)
    {
        $user->delete();

        return redirect()->route('admin.users.index');
    }
}
