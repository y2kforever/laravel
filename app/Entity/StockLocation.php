<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

class StockLocation extends Model
{
    /**
     * @property int $stock_id
     * @property int $location_id
     * @property string $name
     */

    public $timestamps = false;
}
