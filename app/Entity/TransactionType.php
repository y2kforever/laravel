<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

class TransactionType extends Model
{
    /**
     * @property int $id
     * @property string $name
     * @property boolean $status
     * @property string $created_at
     */
}
