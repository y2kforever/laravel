<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

class Token extends Model
{
    /**
     * @property int $id
     * @property string $token
     * @property string $refresh
     * @property string $scope
     * @property boolean $status
     * @property string $life_time
     * @property string $created_at
     * @property string $updated_at
     */
}
