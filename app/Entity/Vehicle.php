<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

class Vehicle extends Model
{
    /**
     * @property int $id
     * @property string $model
     * @property string $mark
     * @property string $number
     * @property boolean $status
     * @property int $color_id
     * @property string $created_at
     */
}
