<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

class CouponsOrder extends Model
{
    /**
     * @property int $coupon_id
     * @property int $order_id
     */

    public $timestamps = false;
}
