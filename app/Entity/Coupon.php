<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

class Coupon extends Model
{
    /**
     * @property int $id
     * @property string $name
     * @property string $description
     * @property int $transaction_id
     * @property boolean $status
     * @property string $start_at
     * @property string $end_at
     * @property string $created_at
     * @property string $updated_at
     */

}
