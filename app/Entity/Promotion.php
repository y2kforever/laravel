<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

class Promotion extends Model
{
    /**
     * @property int $id
     * @property string $name
     * @property string $description
     * @property int $value
     * @property boolean $status
     * @property string $started_at
     * @property string $ended_at
     * @property string $created_at
     * @property string $updated_at
     */
}
