<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

class DevicesAppsRelease extends Model
{
    /**
     * @property int $device_id
     * @property int $app_id
     * @property boolean $status
     */

    public $timestamps = false;
}
