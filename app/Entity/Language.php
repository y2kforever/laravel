<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

class Language extends Model
{
    /**
     * @property int $id
     * @property string $name
     * @property string $iso_code
     */

    public $timestamps = false;
}
