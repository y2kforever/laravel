<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

class DeliveryRating extends Model
{
    /**
     * @property int $delivery_id
     * @property int $rate
     * @property string $comment
     */

    public $timestamps = false;
}
