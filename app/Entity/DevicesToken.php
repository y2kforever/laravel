<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

class DevicesToken extends Model
{
    /**
     * @property int $device_id
     * @property int $token_id
     */

    public $timestamps = false;
}
