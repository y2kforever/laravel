<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

class TransactionLog extends Model
{
    /**
     * @property int $transaction_id
     * @property int $user_id
     */

    public $timestamps = false;
}
