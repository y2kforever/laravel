<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

class DriverStatus extends Model
{
    /**
     * @property int $id
     * @property string $name
     */

    public $timestamps = false;
}
