<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

class Currency extends Model
{
    /**
     * @property int $id
     * @property string $name
     * @property string $code
     * @property string $symbol
     */

    public $timestamps = false;
}
