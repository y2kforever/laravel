<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

class DeviceInfo extends Model
{
    /**
     * @property int $device_id
     * @property string $model
     * @property string $version
     * @property string $etc
     */

    public $timestamps = false;
}
