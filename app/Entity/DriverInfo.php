<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

class DriverInfo extends Model
{
    /**
     * @property int $driver_id
     * @property string $first_name
     * @property string $last_name
     * @property string $parent_name
     * @property string $birth_date
     * @property string $picture
     * @property string $passport_number
     * @property string $start_date
     * @property string $end_date
     * @property string $gender
     */

    public $timestamps = false;
}
