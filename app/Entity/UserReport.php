<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

class UserReport extends Model
{
    /**
     * @property int $user_id
     * @property string $date
     * @property int $order_count
     * @property int $canceled_count
     * @property int $operator_canceled_count
     * @property int $cash_payment_count
     * @property int $cashless_payment_count
     * @property float $cash_payment_cost_sum
     * @property float $cashless_payment_cost_sum
     */

    public $timestamps = false;
}
