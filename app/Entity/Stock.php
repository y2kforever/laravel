<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

class Stock extends Model
{
    /**
     * @property int $id
     * @property string $name
     * @property int $limit
     * @property int $quantity
     * @property string $created_at
     * @property string $updated_at
     */
}
