<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

class UsersChat extends Model
{
    /**
     * @property int $user_id
     * @property int $chat_id
     */

    public $timestamps = false;
}
