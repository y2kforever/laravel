<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

class State extends Model
{
    /**
     * @property int $id
     * @property string $name
     * @property string $slug
     * @property int $country_id
     */

    public $timestamps = false;
}
