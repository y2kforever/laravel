<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

class DriverReport extends Model
{
    /**
     * @property int $driver_id
     * @property string $date
     * @property int $order_count
     * @property int $missed_count
     * @property int $canceled_count
     * @property int $operator_canceled_count
     * @property float $sum_total
     */

    public $timestamps = false;
}
