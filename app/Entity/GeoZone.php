<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

class GeoZone extends Model
{
    /**
     * @property int $id
     * @property string $iso_code
     * @property string $geometry
     * @property int $parent_id
     * @property string $name
     * @property string $description
     * @property boolean $status
     * @property string $created_at
     * @property string $updated_at
     */

}
