<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

class DriversPhone extends Model
{
    /**
     * @property int $driver_id
     * @property int $phone_id
     * @property boolean $main
     * @property int $status_id
     */

    public $timestamps = false;
}
