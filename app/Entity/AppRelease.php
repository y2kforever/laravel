<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

class AppRelease extends Model
{
    /**
     * @property int $id
     * @property int $platform_id
     * @property string $title
     * @property string $checksum
     * @property string $description
     * @property string $created_at
     */
}
