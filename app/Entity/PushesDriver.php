<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

class PushesDriver extends Model
{
    /**
     * @property int $push_id
     * @property int $driver_id
     */

    public $timestamps = false;
}
