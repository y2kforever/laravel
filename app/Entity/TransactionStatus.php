<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

class TransactionStatus extends Model
{
    /**
     * @property int $id
     * @property string $name
     * @property string $created_at
     */
}
