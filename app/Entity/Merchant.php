<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

class Merchant extends Model
{
    /**
     * @property int $id
     * @property string $name
     * @property int $location_id
     * @property string $created_at
     * @property string $updated_at
     */
}
