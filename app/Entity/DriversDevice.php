<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

class DriversDevice extends Model
{
    /**
     * @property int $driver_id
     * @property int $device_id
     */

    public $timestamps = false;
}
