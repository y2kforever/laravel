<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

class OrderStock extends Model
{
    /**
     * @property int $order_id
     * @property int $stock_id
     */

    public $timestamps = false;
}
