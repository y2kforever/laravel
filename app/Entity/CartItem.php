<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

class CartItem extends Model
{
    /**
     * @property int $id
     * @property int $user_id
     * @property int $product_id
     * @property int $quantity
     * @property string $created_at
     * @property string $updated_at
     */
}
