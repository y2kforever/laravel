<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    /**
     * @property int $id
     * @property boolean $status
     * @property string $published_at
     * @property string $created_at
     * @property string $updated_at
     */
}
