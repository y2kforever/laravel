<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

class Chat extends Model
{
    /**
     * @property int $id
     * @property string $chat_topic
     */

    public $timestamps = false;
}
