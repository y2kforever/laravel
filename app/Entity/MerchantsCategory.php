<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

class MerchantsCategory extends Model
{
    /**
     * @property int $merchant_id
     * @property int $category_id
     */

    public $timestamps = false;
}
