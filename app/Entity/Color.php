<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

class Color extends Model
{
    /**
     * @property int $id
     * @property string $name
     * @property string $rgb
     */

    public $timestamps = false;
}
