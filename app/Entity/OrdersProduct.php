<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

class OrdersProduct extends Model
{
    /**
     * @property int $order_id
     * @property int $product_id
     * @property int $quantity
     */

    public $timestamps = false;
}
