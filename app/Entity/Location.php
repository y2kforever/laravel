<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    /**
     * @property int $id
     * @property string $name
     * @property string $slug
     * @property int $city_id
     */

    public $timestamps = false;
}
