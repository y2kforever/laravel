<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    /**
     * @property int $id
     * @property int $user_id
     * @property int $status_id
     * @property int $payment_type_id
     * @property int $app_id
     * @property string $comment
     * @property string $created_at
     * @property string $updated_at
     */
}
