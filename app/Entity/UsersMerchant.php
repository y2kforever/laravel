<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

class UsersMerchant extends Model
{
    /**
     * @property int $user_id
     * @property int $merchant_id
     */

    public $timestamps = false;
}
