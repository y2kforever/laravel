<?php

namespace App\Entity;

use Illuminate\Support\Str;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string $email_verified_at
 * @property string $password
 * @property string $remember_token
 * @property string $verify_token
 * @property string $status_id
 * @property string $created_at
 * @property string $updated_at
 * @property string $blocked_at
 * @property string $unblocked_at
 * @property  UserStatus $status
 */
class User extends Authenticatable
{
    use Notifiable;
    use HasRoles;

    const ROLE_USER = 'user';
    const ROLE_MODERATOR = 'moderator';
    const ROLE_ADMINISTRATOR = 'administrator';

    const STATUS_WAIT = 2;
    const STATUS_ACTIVE = 1;
    const STATUS_BLOCK = 3;

    protected $fillable = [
        'name', 'email', 'password', 'status_id', 'verify_token',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    public static function register(string $name, string $email, string $password): self
    {
        return static::create([
            'name' => $name,
            'email' => $email,
            'password' => bcrypt($password),
            'verify_token' => Str::uuid(),
            'status_id' => self::STATUS_WAIT,
        ]);
    }

    public static function new(string $name, string $email): self
    {
        return static::create([
            'name' => $name,
            'email' => $email,
            'password' => bcrypt(Str::random()),
            'status_id' => self::STATUS_ACTIVE,
        ]);
    }

    public function changeRole($role): void
    {
        if (!$this->hasAnyRole(Role::all())) {
            throw new \InvalidArgumentException('Undefined role "' . $role . '"');
        }
        /*if ($this->role === $role) {
            throw new \DomainException('Role is already assigned.');
        }*/
        $this->assignRole($role);
    }

    public function isWait(): bool
    {
        return $this->status_id === self::STATUS_WAIT;
    }

    public function isActive(): bool
    {
        return $this->status_id === self::STATUS_ACTIVE;
    }

    public function isModerator(): bool
    {
        return $this->hasRole(self::ROLE_MODERATOR);
    }

    public function isAdmin(): bool
    {
        return $this->hasRole(self::ROLE_ADMINISTRATOR);
    }

    public static function rolesList(): array
    {
        return Role::all()->toArray();
    }

    public function verify(): void
    {
        if (!$this->isWait()) {
            throw new \DomainException('User is already verified.');
        }

        $this->update([
            'status_id' => self::STATUS_ACTIVE,
            'verify_token' => null,
        ]);
    }

    public function status()
    {
        return $this->hasOne(UserStatus::class, 'id', 'status_id');
    }


}
