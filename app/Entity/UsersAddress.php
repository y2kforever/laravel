<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

class UsersAddress extends Model
{
    /**
     * @property int $id
     * @property int $user_id
     * @property string $address
     * @property string $coordinate
     * @property int $location_id
     */

    public $timestamps = false;
}
