<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

class PromotionsOrder extends Model
{
    /**
     * @property int $promotion_id
     * @property int $order_id
     */

    public $timestamps = false;
}
