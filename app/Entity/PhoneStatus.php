<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

class PhoneStatus extends Model
{
    /**
     * @property int $id
     * @property string $name
     * @property string $description
     */

    public $timestamps = false;
}
