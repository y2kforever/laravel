<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

class UsersDevice extends Model
{
    /**
     * @property int $user_id
     * @property int $device_id
     * @property string $last_active
     */

    public $timestamps = false;
}
