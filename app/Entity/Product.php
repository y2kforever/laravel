<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    /**
     * @property int $id
     * @property string $name
     * @property int $merchant_id
     * @property float $price
     * @property int $currency_id
     * @property int $status_id
     * @property float $volume
     * @property float $weight
     * @property string $picture
     * @property string $created_at
     * @property string $updated_at
     */
}
