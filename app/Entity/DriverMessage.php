<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

class DriverMessage extends Model
{
    /**
     * @property int $id
     * @property int $driver_id
     * @property boolean $status
     * @property string $message
     * @property string $sent_to
     * @property string $created_at
     */

}
