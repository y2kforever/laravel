<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

class UsersPhone extends Model
{
    /**
     * @property int $user_id
     * @property int $phone_id
     * @property boolean $main
     * @property int $status_id
     */

    public $timestamps = false;
}
