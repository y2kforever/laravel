<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

class VehiclesDriver extends Model
{
    /**
     * @property int $vehicle_id
     * @property int $driver_id
     */

    public $timestamps = false;
}
