<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

class ProductsCategory extends Model
{
    /**
     * @property int $product_id
     * @property int $category_id
     */

    public $timestamps = false;
}
