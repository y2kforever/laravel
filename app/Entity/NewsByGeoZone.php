<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

class NewsByGeoZone extends Model
{
    /**
     * @property int $geo_zone_id
     * @property int $news_id
     */

    public $timestamps = false;
}
