<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

class ProductsAttribute extends Model
{
    /**
     * @property int $product_id
     * @property int $attribute_id
     */

    public $timestamps = false;
}
