<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

class OrderAmount extends Model
{
    /**
     * @property int $order_id
     * @property float $order_price
     * @property float $delivery_price
     */

    public $timestamps = false;
}
