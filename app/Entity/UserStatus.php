<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

class UserStatus extends Model
{
    /**
     * @property int $id
     * @property string $name
     * @property string $description
     * @property string $created_at
     */
}
