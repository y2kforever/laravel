<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

class Phone extends Model
{
    /**
     * @property int $id
     * @property string $phone
     * @property string $created_at
     */
}
