<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

class PushesUser extends Model
{
    /**
     * @property int $push_id
     * @property int $user_id
     */

    public $timestamps = false;
}
