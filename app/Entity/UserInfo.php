<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

class UserInfo extends Model
{
    /**
     * @property int $user_id
     * @property string $first_name
     * @property string $last_name
     * @property string $parent_name
     * @property string $birth_date
     * @property string $gender
     */

    public $timestamps = false;
}
