<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

class Platform extends Model
{
    /**
     * @property int $id
     * @property string $title
     * @property string $url
     * @property string $signature
     */

    public $timestamps = false;
}
