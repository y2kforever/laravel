<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

class Attribute extends Model
{
    /**
     * @property int $id
     * @property int $category_id
     * @property string $name
     * @property string $description
     * @property string $type
     * @property boolean $required
     * @property string $variants
     * @property boolean $status
     * @property int $sort
     * @property string $created_at
     * @property string $updated_at
     */
}
