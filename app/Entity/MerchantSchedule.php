<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

class MerchantSchedule extends Model
{
    /**
     * @property int $merchant_id
     * @property string $name
     * @property boolean $lunch
     * @property int $week_day
     * @property string $start_time
     * @property string $end_time
     */

    public $timestamps = false;
}
