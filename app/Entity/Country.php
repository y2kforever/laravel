<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    /**
     * @property int $id
     * @property string $name
     * @property string $slug
     */

    public $timestamps = false;
}
