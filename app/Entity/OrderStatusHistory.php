<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

class OrderStatusHistory extends Model
{
    /**
     * @property int $order_id
     * @property int $status_id
     * @property int $administrator_id
     * @property string $created_at
     */

}
