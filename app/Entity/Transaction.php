<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    /**
     * @property int $id
     * @property int $type_id
     * @property int $status_id
     * @property string $created_at
     * @property string $updated_at
     */
}
