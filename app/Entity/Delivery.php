<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

class Delivery extends Model
{
    /**
     * @property int $id
     * @property int $order_id
     * @property int $driver_id
     * @property int $delivery_address_id
     * @property string $comment
     */

    public $timestamps = false;
}
