<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

class SmsSend extends Model
{
    /**
     * @property int $id
     * @property string $phone
     * @property string $body
     * @property boolean $status
     * @property string $sent_to
     * @property string $created_at
     */
}
