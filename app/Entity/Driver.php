<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

class Driver extends Model
{
    /**
     * @property int $id
     * @property int $status_id
     * @property string $blocked_at
     * @property string $unblocked_at
     * @property string $created_at
     */
}
