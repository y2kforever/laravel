<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

class Push extends Model
{
    /**
     * @property int $id
     * @property string $started_at
     * @property string $ended_at
     * @property int $interval
     * @property string $body
     * @property int $status_id
     * @property string $created_at
     * @property string $updated_at
     */
}
