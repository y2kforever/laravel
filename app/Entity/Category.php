<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    /**
     * @property int $id
     * @property string $name
     * @property string $slug
     * @property string $description
     * @property string $picture
     * @property int $_lft
     * @property int $_rgt
     * @property int $parent_id
     * @property boolean $status
     * @property string $created_at
     * @property string $updated_at
     */

    public $timestamps = false;
}
