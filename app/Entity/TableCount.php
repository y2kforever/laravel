<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

class TableCount extends Model
{
    /**
     * @property string $name
     * @property int $count_rows
     */

    public $timestamps = false;
}
