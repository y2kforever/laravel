<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

class OrderRating extends Model
{
    /**
     * @property int $order_id
     * @property int $rate
     * @property string comment
     */

    public $timestamps = false;
}
