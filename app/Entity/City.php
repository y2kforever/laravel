<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    /**
     * @property int $id
     * @property string $name
     * @property string $slug
     * @property int $state_id
     * @property string $index
     */

    public $timestamps = false;
}
