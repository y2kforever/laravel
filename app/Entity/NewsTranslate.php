<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

class NewsTranslate extends Model
{
    /**
     * @property int $news_id
     * @property int $lang_id
     * @property string $title
     * @property string $description
     */

    public $timestamps = false;
}
