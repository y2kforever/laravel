Installation
------------

- The minimum required PHP version is PHP 7.1.0
- The minimum required MySQL version is 5.7

Installation with docker
------------

make docker-build

Documentation
-------------


### Reporting Security issues


### Directory Structure

```
app/               internally used build tools
bootstrap/         documentation
config/            core framework code
core/              Core 
database/          tests of the core framework code
docker/            tests of the core framework code
public/            tests of the core framework code
resources/         tests of the core framework code
routes/            tests of the core framework code
storage/           tests of the core framework code
```

### Spreading the Word


**In presentations**

If you are giving a presentation or talk featuring work that makes use of Yii 2 and would like to acknowledge it,
we suggest using [our logo](https://www.yiiframework.com/logo/) on your title slide.

**In projects**
