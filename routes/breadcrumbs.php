<?php
/**
 * Created by PhpStorm.
 * User: abdugania
 * Date: 12/12/18
 * Time: 12:41 AM
 */

use App\Entity\User;
use Spatie\Permission\Models\{Role, Permission};
use DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs;
use DaveJamesMiller\Breadcrumbs\BreadcrumbsGenerator as Crumbs;

Breadcrumbs::register('home', function (Crumbs $crumbs) {
    $crumbs->push('Home', route('home'));
});

Breadcrumbs::register('login', function (Crumbs $crumbs) {
    $crumbs->parent('home');
    $crumbs->push('Login', route('login'));
});

Breadcrumbs::register('register', function (Crumbs $crumbs) {
    $crumbs->parent('home');
    $crumbs->push('Register', route('register'));
});

Breadcrumbs::register('password.request', function (Crumbs $crumbs) {
    $crumbs->parent('login');
    $crumbs->push('Reset Password', route('password.request'));
});

Breadcrumbs::register('password.reset', function (Crumbs $crumbs) {
    $crumbs->parent('password.request');
    $crumbs->push('Change', route('password.reset'));
});

Breadcrumbs::register('cabinet', function (Crumbs $crumbs) {
    $crumbs->parent('home');
    $crumbs->push('Cabinet', route('cabinet'));
});

Breadcrumbs::register('admin.home', function (Crumbs $crumbs) {
    $crumbs->parent('home');
    $crumbs->push('Admin', route('admin.home'));
});

Breadcrumbs::register('admin.users.index', function (Crumbs $crumbs) {
    $crumbs->parent('admin.home');
    $crumbs->push('Users', route('admin.users.index'));
});

Breadcrumbs::register('admin.users.create', function (Crumbs $crumbs) {
    $crumbs->parent('admin.users.index');
    $crumbs->push('Create', route('admin.users.create'));
});

Breadcrumbs::register('admin.users.show', function (Crumbs $crumbs, User $user) {
    $crumbs->parent('admin.users.index');
    $crumbs->push($user->name, route('admin.users.show', $user));
});

Breadcrumbs::register('admin.users.edit', function (Crumbs $crumbs, User $user) {
    $crumbs->parent('admin.users.show', $user);
    $crumbs->push('Edit', route('admin.users.edit', $user));
});

Breadcrumbs::register('admin.roles.index', function (Crumbs $crumbs) {
    $crumbs->parent('admin.home');
    $crumbs->push('Roles', route('admin.roles.index'));
});

Breadcrumbs::register('admin.roles.create', function (Crumbs $crumbs) {
    $crumbs->parent('admin.roles.index');
    $crumbs->push('Create', route('admin.roles.create'));
});

Breadcrumbs::register('admin.roles.show', function (Crumbs $crumbs, Role $roles) {
    $crumbs->parent('admin.roles.index');
    $crumbs->push($roles->name, route('admin.roles.show', $roles));
});

Breadcrumbs::register('admin.roles.edit', function (Crumbs $crumbs, Role $roles) {
    $crumbs->parent('admin.roles.show', $roles);
    $crumbs->push('Edit', route('admin.roles.edit', $roles));
});

Breadcrumbs::register('admin.permissions.index', function (Crumbs $crumbs) {
    $crumbs->parent('admin.home');
    $crumbs->push('Permissions', route('admin.permissions.index'));
});

Breadcrumbs::register('admin.permissions.create', function (Crumbs $crumbs) {
    $crumbs->parent('admin.permissions.index');
    $crumbs->push('Create', route('admin.permissions.create'));
});

Breadcrumbs::register('admin.permissions.show', function (Crumbs $crumbs, Permission $permission) {
    $crumbs->parent('admin.permissions.index');
    $crumbs->push($permission->name, route('admin.permissions.show', $permission));
});

Breadcrumbs::register('admin.permissions.edit', function (Crumbs $crumbs, Permission $permission) {
    $crumbs->parent('admin.permissions.show', $permission);
    $crumbs->push('Edit', route('admin.permissions.edit', $permission));
});