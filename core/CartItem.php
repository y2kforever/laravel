<?php
/**
 * Created by PhpStorm.
 * User: Abdugani Adikhanov
 * Email: y2k.gostop@gmail.com
 * Date: 23/11/18
 * Time: 12:42 AM
 */

namespace core;

class CartItem
{
    private $id;
    private $count;
    private $price;

    public function __construct($id, $count, $price)
    {
        $this->id = $id;
        $this->count = $count;
        $this->price = $price;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getCount()
    {
        return $this->count;
    }

    public function getPrice()
    {
        return $this->price;
    }

    public function getCost()
    {
        return $this->price * $this->count;
    }
}