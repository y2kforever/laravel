<?php
/**
 * Created by PhpStorm.
 * User: Abdugani Adikhanov
 * Email: y2k.gostop@gmail.com
 * Date: 22/11/18
 * Time: 12:07 PM
 */

namespace core\cost;

class FridayCost implements CalculatorInterface
{
    private $next;
    private $date;
    private $percent;

    public function __construct(CalculatorInterface $next, $percent, $date)
    {
        $this->next = $next;
        $this->date = $date;
        $this->percent = $percent;
    }

    public function getCost(array $items)
    {

        $now = \DateTime::createFromFormat('Y-n-d', $this->date);

        if ($now->format('l') == 'Friday') {
            return (1 - $this->percent / 100) * $this->next->getCost($items);
        } else {
            return $this->next->getCost($items);
        }
    }

}