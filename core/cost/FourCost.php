<?php
/**
 * Created by PhpStorm.
 * User: Abdugani Adikhanov
 * Email: y2k.gostop@gmail.com
 * Date: 22/11/18
 * Time: 12:15 PM
 */

namespace core\cost;

class FourCost implements CalculatorInterface
{
    private $next;

    public function __construct(CalculatorInterface $next)
    {
        $this->next = $next;
    }

    public function getCost(array $items)
    {

        $cost = $this->next->getCost($items);

        $k = 0;
        foreach ($items as $item) {
            if ($k % 4 === 3) {
                $cost -= $item->getCost() - 1;
            }
            $k++;
        }
        return $cost;
    }
}