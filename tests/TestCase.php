<?php
/**
 * Created by PhpStorm.
 * User: abdugania
 * Date: 19/12/18
 * Time: 3:47 PM
 */

namespace Tests;

use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;
}
