<?php
/**
 * Created by PhpStorm.
 * User: abdugania
 * Date: 19/12/18
 * Time: 4:13 PM
 */

namespace Tests\Unit\Entity\User;

use Tests\TestCase;
use App\Entity\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class CreateTest extends TestCase
{
    use DatabaseTransactions;

    public function testNew(): void
    {
        $user = User::new(
            $name = 'name',
            $email = 'email'
        );

        self::assertNotEmpty($user);

        self::assertEquals($name, $user->name);
        self::assertEquals($email, $user->email);
        self::assertNotEmpty($user->password);

        self::assertTrue($user->isActive());

    }

}