@extends('layouts.app')

@section('content')
    @include('admin.roles._nav')

    <p><a href="{{ route('admin.roles.create') }}" class="btn btn-success">Add Role</a></p>

    <div class="card mb-3">
        <div class="card-header">Filter</div>
        <div class="card-body">
            <form action="?" method="GET">
                <div class="row">
                    <div class="col-sm-1">
                        <div class="form-group">
                            <label for="id" class="col-form-label">ID</label>
                            <input id="id" class="form-control" name="id" value="{{ request('id') }}">
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label for="name" class="col-form-label">Name</label>
                            <input id="name" class="form-control" name="name" value="{{ request('name') }}">
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label for="guard_name" class="col-form-label">Guard Name</label>
                            <input id="guard_name" class="form-control" name="guard_name"
                                   value="{{ request('guard_name') }}">
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label class="col-form-label">&nbsp;</label><br/>
                            <button type="submit" class="btn btn-primary">Search</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <table class="table table-bordered table-striped">
        <thead>
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Guard Name</th>
            <th>Created At</th>
            <th>Updated At</th>
            <th>&nbsp;</th>
        </tr>
        </thead>
        <tbody>

        @foreach ($roles as $role)
            <tr>
                <td>{{ $role->id }}</td>
                <td><a href="{{ route('admin.roles.show', $role) }}">{{ $role->name }}</a></td>
                <td>{{ $role->guard_name }}</td>
                <td>
                    <span class="badge badge-success">{{ $role->created_at }}</span>
                </td>
                <td>
                    <span class="badge badge-success">{{ $role->updated_at }}</span>
                </td>
                <td>
                    <a href="{{ route('admin.roles.edit',[$role]) }}" class="btn btn-info">@lang('Edit')</a>
                </td>
            </tr>
        @endforeach

        </tbody>
    </table>
@stop

@section('javascript')
    <script>
        {{--window.route_mass_crud_entries_destroy = '{{ route('admin.roles.mass_destroy') }}';--}}
    </script>
@endsection