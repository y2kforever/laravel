@extends('layouts.app')

@section('content')
    @include('admin.roles._nav')

    <h3 class="page-title">@lang('global.roles.title')</h3>

    <form method="POST" action="{{ route('admin.roles.store') }}">
        @csrf

        <div class="form-group">
            <label for="name" class="col-form-label">Name</label>
            <input id="name" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name"
                   value="{{ old('name') }}" required>
            @if ($errors->has('name'))
                <span class="invalid-feedback"><strong>{{ $errors->first('name') }}</strong></span>
            @endif
        </div>
        <div class="form-group">
            <label for="name" class="col-form-label">Permission</label>
            <input id="permission" class="form-control{{ $errors->has('permission') ? ' is-invalid' : '' }}" name="permission" value="{{ old('permission') }}" required>
            @if ($errors->has('permission'))
                <span class="invalid-feedback"><strong>{{ $errors->first('permission') }}</strong></span>
            @endif

        </div>

        <div class="form-group">
            <button type="submit" class="btn btn-primary">Save</button>
        </div>
    </form>
@stop

